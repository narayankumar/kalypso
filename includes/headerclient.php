<?php 
function kalypso_headerclient($page){
  global $root; 
?>

    <div class="container">
      <div class="row">  
        <div class="span2"> 
          <div id="vert_nav">
          <header>
          <?php if (theme_get_setting('branding_type') == 'logo'): ?>
          <div id="main_title">
            <a href="<?php print base_path();?>"><img src="<?php print file_create_url(theme_get_setting('bg_path')); ?>" /></a>
          </div>
          <?php endif; ?>
          
          <?php if (theme_get_setting('branding_type') == 'text'): ?>
            <a href="<?php print base_path();?>">
            <div id="main_title">
              <h1 id="main_title_text"><?php print variable_get('site_name'); ?></h1>
            </div>
            </a>
          <?php endif; ?>
                
            </header> 
          </div> 
        </div>
        <!-- end main span2 -->  
          
<?php }
?>